## Django REST with React simple CRM

Django REST framework with React as frontend to build a simple responsive Web app to list, search, create, update and delete customers.

## Requirements to deploy

* Check the dependencies in files package.json (Bootstrap, axios, react-dom, react-scripts, react-router-dom).

## Deployment

1. Clone project.
2. Install Django, djangorestframework, django-cors-headers, nodejs and dependencies.
3. Run python manage.py runserver and npm start to serve Django + React together in dev mode.
4. For production, you can use Gunicorn + Nginx + Django + React + Webpack.
