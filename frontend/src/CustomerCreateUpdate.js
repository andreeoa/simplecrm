import React, { Component, createRef } from 'react';
import CustomersService from './CustomersService';

const customersService = new CustomersService();

class CustomerCreateUpdate extends Component {
    constructor(props){
        super(props);

        this.state = {
            message: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.firstName = createRef();
        this.lastName = createRef();
        this.email = createRef();
        this.phone = createRef();
        this.address = createRef();
        this.description = createRef();
    }

    componentDidMount(){
        const { match: {params} } = this.props;
        if(params && params.pk)
        {
          customersService.getCustomer(params.pk).then((c)=>{
            this.firstName.current.value = c.first_name;
            this.lastName.current.value = c.last_name;
            this.email.current.value = c.email;
            this.phone.current.value = c.phone;
            this.address.current.value = c.address;
            this.description.current.value = c.description;
          })
        }
    }

    handleCreate(){
        customersService.createCustomer(
        {
            "first_name": this.firstName.current.value,
            "last_name": this.lastName.current.value,
            "email": this.email.current.value,
            "phone": this.phone.current.value,
            "address": this.address.current.value,
            "description": this.description.current.value
          }
        ).then((result)=>{
            this.setState({ message: 'Customer created'})
        }).catch(()=>{
            this.setState({ message: 'There was an error. Please re-check your form.'})
        });
    }

    handleUpdate(pk){
        customersService.updateCustomer(
          {
            "pk": pk,
            "first_name": this.firstName.current.value,
            "last_name": this.lastName.current.value,
            "email": this.email.current.value,
            "phone": this.phone.current.value,
            "address": this.address.current.value,
            "description": this.description.current.value
        }
        ).then((result)=>{
            this.setState({ message: 'Customer updated'})
        }).catch(()=>{
            this.setState({ message: 'There was an error. Please re-check your form.'})
        });
    }

    handleSubmit(event) {
        const { match: {params} } = this.props;

        if(params && params.pk){
          this.handleUpdate(params.pk);
        }
        else
        {
          this.handleCreate();
        }
        event.preventDefault();
    }

    render() {
        return (
          <div>
          <form onSubmit={this.handleSubmit} className="form-add">
          <div className="form-group">
              <label className="sr-only">First name</label>
              <input className="form-control" type="text" ref={this.firstName} placeholder="First name" required/>
          </div>
          <div className="form-group">
              <label className="sr-only">Last name</label>
              <input className="form-control" type="text" ref={this.lastName} placeholder="Last name" required/>
          </div>
          <div className="form-group">
              <label className="sr-only">Telephone</label>
              <input className="form-control" type="number" ref={this.phone} placeholder="Telephone" required/>
          </div>
          <div className="form-group">
              <label className="sr-only">Email</label>
              <input className="form-control" type="email" ref={this.email} placeholder="Email" required/>
          </div>
          <div className="form-group">
              <label className="sr-only">Address</label>
              <input className="form-control" type="text" ref={this.address} placeholder="Address"/>
          </div>
          <div className="form-group">
              <label className="sr-only">Description</label>
              <textarea className="form-control" ref={this.description} placeholder="Description"></textarea>
          </div>
          <div className="form-group">
            <input className="btn btn-primary" type="submit" value="Submit" />
          </div>
          </form>
          {this.state.message}
          </div>
        );
    }
}
export default CustomerCreateUpdate;