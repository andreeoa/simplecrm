import  React, { Component, createRef } from  'react';
import  CustomersService  from  './CustomersService';

const  customersService  =  new  CustomersService();

class  CustomersList  extends  Component {

    constructor(props) {
        super(props);
        this.state  = {
            customers: [],
            nextPageURL:  '',
            queryResult: '',
        };
        this.nextPage  =  this.nextPage.bind(this);
        this.handleDelete  =  this.handleDelete.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.queryInput = createRef();
    }

    componentDidMount() {
        customersService.getCustomers().then((result) => {
            this.setState({ customers: result.data, nextPageURL: result.nextlink})
        });
    }

    handleDelete(e,pk){
        customersService.deleteCustomer({pk : pk}).then(()=>{
            var newArr = this.state.customers.filter(function(obj) {
                return obj.pk !== pk;
            });
            this.setState({customers: newArr})
        });
    }

    nextPage(){
        customersService.getCustomersByURL(this.state.nextPageURL).then((result) => {
            this.setState({customers: result.data, nextPageURL: result.nextlink})
        });
    }

    handleSubmit(event){
        if (this.queryInput.current.value !== ""){
            customersService.getCustomersByQuery(this.queryInput.current.value).then((result) => {
                this.setState({queryResult: result});
            });
        }
        event.preventDefault();
    }

    handleChange(event) {
        if (this.queryInput.current.value === ""){
            this.setState({queryResult: []});
        }
    }

    renderTable(){

        const tableAllCustomers = (
            <>
            <table className="table">
				<thead  key="thead">
				<tr>
				    <th>#</th>
				    <th>First Name</th>
				    <th>Last Name</th>
				    <th>Phone</th>
				    <th>Email</th>
				    <th>Address</th>
				    <th>Description</th>
				    <th>Actions</th>
				</tr>
				</thead>
				<tbody>
				    {this.state.customers.map( c  =>
				    <tr  key={c.pk}>
					<td>{c.pk}  </td>
					<td>{c.first_name}</td>
					<td>{c.last_name}</td>
					<td>{c.phone}</td>
					<td>{c.email}</td>
					<td>{c.address}</td>
					<td>{c.description}</td>
					<td>
					    <button type="button" class="btn btn-danger" onClick={(e)=>  this.handleDelete(e,c.pk) }>Delete</button>
					    <a  href={"/customer/" + c.pk}> Update</a>
					</td>
				    </tr>)}
				</tbody>
			</table>
			<div className="text-center">
                <button className="btn btn-primary" onClick= {this.nextPage}>Next</button>
            </div>
            </>
        );

        if (this.state.queryResult){
            if (this.state.queryResult.length) {
                return (
                <>
                <table className="table">
				<thead key="thead">
				<tr>
				    <th>#</th>
				    <th>First Name</th>
				    <th>Last Name</th>
				    <th>Phone</th>
				    <th>Email</th>
				    <th>Address</th>
				    <th>Description</th>
				</tr>
				</thead>
				<tbody>
				    {this.state.queryResult.map( c  =>
				    <tr key={c.pk}>
					<td>{c.pk}  </td>
					<td>{c.first_name}</td>
					<td>{c.last_name}</td>
					<td>{c.phone}</td>
					<td>{c.email}</td>
					<td>{c.address}</td>
					<td>{c.description}</td>
				    </tr>)}
				</tbody>
			    </table>
                </>
                );
            }else{
                return (tableAllCustomers);
            }
        }else{
			return (tableAllCustomers);
    	}
    }

    render() {
        return (
        <div  className="customers--list">
            <form className="form-inline d-flex justify-content-center active-blue" onSubmit={this.handleSubmit}>
                <input className="form-control form-control-sm w-75" type="text" defaultValue="" ref={this.queryInput} placeholder="Search" aria-label="Search" onChange={this.handleChange}/>
                <button className="ml-1" type="submit"><i className="fa fa-search" aria-hidden="true"></i></button>
            </form>
            {this.renderTable()}
        </div>
        );
    }
}
export  default  CustomersList;
